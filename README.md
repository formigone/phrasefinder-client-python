# PhraseFinder Python Client

[![pipeline status](https://gitlab.com/mtrenkmann/phrasefinder-client-python/badges/master/pipeline.svg)](https://gitlab.com/mtrenkmann/phrasefinder-client-python/commits/master)

[PhraseFinder](https://phrasefinder.io) is a search engine for the [Google Books Ngram Dataset](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html) (version 2). This repository contains the official Python client for requesting PhraseFinder's web [API](https://phrasefinder.io/api) which is free to use for any purpose.

* [Documentation](https://phrasefinder.io/documentation)
* [Python API Reference](https://mtrenkmann.gitlab.io/phrasefinder-client-python/)

## Install via pip

```sh
pip install git+https://gitlab.com/mtrenkmann/phrasefinder-client-python.git
```

## Simple request example

```python
from __future__ import print_function
from phrasefinder import phrasefinder as pf

def main():
    """
    Performs a simple request and prints out the result.
    """

    # Set up your query.
    query = 'I like ???'

    # Optional: set the maximum number of phrases to return.
    options = pf.SearchOptions()
    options.topk = 10

    # Send the request.
    try:
        result = pf.search(pf.Corpus.AMERICAN_ENGLISH, query, options)
        if result.error_message:
            print('Request was not successful: {}'.format(result.error_message))
            return

        # Print phrases line by line.
        for phrase in result.phrases:
            print("{0:6f}".format(phrase.score), end="")
            for token in phrase.tokens:
                print(" {}".format(token.text), end="")
            print()

    except Exception as error:
        print('Fatal error: {}'.format(error))

if __name__ == '__main__':
    main()
```

## Run the example

```sh
cd phrasefinder-client-python
PYTHONPATH=src python examples/simple_request.py
```

*There are various methods setting PYTHONPATH. This one works on Linux.*

## Output

```plain
0.175468 I like to think of
0.165350 I like to think that
0.149246 I like it . "
0.104326 I like it , "
0.091746 I like the way you
0.082627 I like the idea of
0.064459 I like that . "
0.057900 I like it very much
0.055201 I like you . "
0.053677 I like the sound of
```

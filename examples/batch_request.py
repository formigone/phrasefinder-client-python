#!/usr/bin/env python

from __future__ import print_function
import phrasefinder as pf

def main():
    """
    Performs a batch request and prints out the result.
    """

    batch = pf.BatchRequest()

    # Set parameters relevant to all requests.
    batch.params = {'key'   : 'your-secret-api-key',
                    'corpus': pf.Corpus.AMERICAN_ENGLISH,
                    'topk'  : 3}

    # Add requests.
    batch.requests.append({'query': 'I like ???'})
    batch.requests.append({'query': 'I hate ???', 'topk': 4})
    batch.requests.append({'query': 'a single " is invalid'})

    # Send the batch request.
    try:
        results = batch.execute()
        for request, result in zip(batch.requests, results):
            print("query: {}".format(request['query']))
            if result.error_message:
                print('Request was not successful: {}'.format(result.error_message))
            else:
                for phrase in result.phrases:
                    print("{0:6f}".format(phrase.score), end="")
                    for token in phrase.tokens:
                        print(" {}".format(token.text), end="")
                    print()
            print()

    except Exception as error:
        print('Fatal error: {}'.format(error))

if __name__ == '__main__':
    main()

# Output
#
# query: I like ???
# 0.358050 I like to think of
# 0.337406 I like to think that
# 0.304544 I like it . "
#
# query: I hate ???
# 0.298744 I hate to say it
# 0.248237 I hate to admit it
# 0.227041 I hate you ! "
# 0.225978 I hate to think of
#
# query: a single " is invalid
# Request was not successful: malformed query (missing closing quote)

from setuptools import setup

setup(
   name='phrasefinder',
   version='0.7.0',
   packages=['phrasefinder'],
   package_dir={'phrasefinder': 'phrasefinder'},
   install_requires=['requests'],
)

